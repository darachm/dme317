#!/usr/bin/env nextflow
nextflow.enable.dsl=2
// This specifies that we're using the DSL2 version
// Strangely, this comment not be on the same line as the directive

/* Introduction and preliminaries */

/* Pipeline for analyzing the barcodes for the glabrata library.
 * Chopping them up, cluster them, and then output a table of these to 
 * analyze in R.
 *
 * To input, run with the argument `--reads 'path/to/fastq/*fqz' `
 * Note the quotes! That's important, so that it gets sent as a string,
 * then can interpret it as a glob later.
 *
 * Nextflow is written in groovy, a variant of java. So comments begin with
 * two slashes, or you can use slash star and star slash, as shown here.
 * Yes, this is a comment.
 */

/* TODO list
    itermae - gotta be clear
    figure starcode vs bartender, lev or hamming
    figure outputs nicely
    does fastqc take paired end data?
*/

// Making directories to store output files, reports, and some tmp links for
// manual inspection
file("./tmp").mkdirs()
file("./output").mkdirs()
file("./reports").mkdirs()

/* Amplicon structure:
 * with Katja's primer it's like so
 * AATGATACGGCGACCACCGAGATCTACACTCTTTCCCTACACGACGCTCTTCCGATCT
 * read1 starts here, 5' to 3'
 * 0-6 bases of index, then
 * TTAATATGGACTAAAGGAGGCTTTTGTCGACGGATCCGATATCGGTACC
 * NNNNNAANNNNNTTNNNNNTTNNNNNATAACTTCGTATAATGTATGCTATACGAAGTTATTGC
 * GCGGTGATCACTTATGGtaccgTTCGTATAAaGTaTcCTATACGAACGGTATGCGCGGTGATCACTTAT
 * GGTACCGTTCGTATAATGTGTACTATACGAACGGTAANNNNNAANNNNNTTNNNNNTTNNNNN
 * GGTACCGATATCAGATCTAAGCTTGAATTCGA
 * 0-6 bases of index, then
 * AGATCGGAAGAGCGGTTCAGCAGGAATGCCGAGACCGATCTCGTATGCCGTCTTCTGCTTG
 *
 * read1 patterns
 * [ATCGN]+
 * TTAATATGGACTAAAGGAGGCTTTTGTCGACGGATCCGATATCGGTACC
 * NNNNNAANNNNNTTNNNNNTTNNNNN
 * ATAACTTCGTATAATGTATGCTATACGAAGTTATTGC
 *
 * read2 patterns, by echo "" | rev | tr 'ATCG' 'tagc' 
 * [ATCGN]+
 * TCGAATTCAAGCTTAGATCTGATATCGGTACC
 * NNNNNAANNNNNAANNNNNTTNNNNN
 * TTACCGTTCGTATAGTACACATTATACGAACGGTACC
 */

itermae_arguments = 
    [
        [   "1", // which read to join on
            '-o "input > ^(?<umi>[ATCGN]{7,9}?)(?<sample>[ATCGN]{6,6})(?P<fixed>TTAATATGGA){e<=1}(?<rest>[ATCGN]*)$" '+
            '-o "rest  > (?P<upPrime>TATCGGTACC){e<=1}(?P<barcode>[ATCGN]{22,30}?)(?P<downPrime>ATAACTTCGT){e<=1}" '+
            '--filter "statistics.median(barcode.quality)>=30" '+
            '--output-id "input.id" -oseq "sample" '+
            '--output-id "input.id" -oseq "barcode" '+
            '-of "sam" '+
            ''],
        [   "2",
            '-o "input > ^(?<umi>[ATCGN]{7,9}?)(?<sample>[ATCGN]{9,9})(?P<fixed>TCGAATTCAA){e<=1}(?<rest>[ATCGN]*)$" '+
            '-o "rest  > (?P<upPrime>TATCGGTACC){e<=1}(?P<barcode>[ATCGN]{22,30}?)(?P<downPrime>TTACCGTTCG){e<=1}" '+
            '--filter "statistics.median(barcode.quality)>=30" '+
            '--output-id "input.id" -oseq "sample" '+
            '--output-id "input.id" -oseq "barcode" '+
            '-of "sam" '+
            '']
        ]


/*
            '-o "upPrime  > (?P<upPad>.{4,4})$" '+
            '-o "downPrime  > ^(?P<downPad>.{4,4})" '+
*/

/* Executing the workflow. */

workflow {

    // Read, parse name into ID and which read it is, 1 is forward
    input_fastqz = Channel.fromPath(params.reads) // this finds those files
        .map{ (file, run, lane, read) = (it =~ /([^\/]+)_(L\d+)_[Rr]?(\d+).f.*z/)[0]
            [ read, run+'_'+lane, it] } 
        // So this returns [ which read , ID , path to it ] 
        // for each pair of fastqs

    // QC
    // This mess is just to pair up the paired end reads by ID
    input_fastqz_paired = input_fastqz.groupTuple(by: 1) 
        .map{   if ( it[0][0] == "1" ) { reads = [ it[2][0] , it[2][1] ] } 
                else { reads = [ it[2][1] , it[2][0] ] }
                [ it[1], reads[0], reads[1] ] 
        } 
    fastqc(input_fastqz).collect() | multiqc
    fastp(input_fastqz_paired)

    // adding itermae arguments from itermae_arguments object
    input_fastqz.join(Channel.fromList(itermae_arguments),by: 0) \
        | itermae 
        | split_out_codes \
        | transpose \
        | map{ it -> [] + ( it[2] =~ /.*\/(.*).codes/ )[0][1] + it }
        | set{ itermae_outputs }

    Channel.of(
            ['XI:0','starcode','-d 1 -s '],
            ['XI:1','bartender','-c 1 -z 5 -d 5 -l 3']
            ) \
        | cross(itermae_outputs) \
        | map{ it -> it[0]+it[1][1..-1] } \
        | branch{ 
                for_starcode: it[1] == 'starcode'
                for_bartender: it[1] == 'bartender'
            } \
        | set{ extracted_codes }

    extracted_codes.for_starcode \
        | starcode \
        | set{ starcoded }

    extracted_codes.for_bartender \
        | bartender | unfold_bartender \
        | set{ bartended }

    starcoded.mix(bartended) \
        | groupTuple(by: [0]) \
        | remerge_codes \
        | tabulate_barcode_combos
        | view 

}

/* Defining these processes */

// Runs fastqc on a fastq file
process fastqc {
    cpus 1
    memory '8 GB'
    label 'qc'
    input:
        tuple val(read), val(id), path(fastqz)
    output:
        path("${fastqz}_qc") 
    shell: 
        """
        mkdir ${fastqz}_qc
        fastqc --threads ${task.cpus} -o ${fastqz}_qc ${fastqz}
        """
}

// Compile FASTQC outputs into a report, publish
process multiqc {
    publishDir 'output'
    cpus 1
    memory '2 GB'
    label 'qc'
    input:
        path("*")
    output:
        path("*")
    shell: 
        '''
        multiqc ./
        '''
}

// running fastp, which is sort of an alternative to fastqc with more deets
process fastp {
    publishDir 'output'
    cpus 1
    memory '8 GB'
    label 'fastp'
    input:
        tuple val(id), path(read1), path(read2)
    output:
        path("*")
    shell: 
        """
        /opt/fastp --in1 ${read1} --in2 ${read2} --overrepresentation_analysis
        """
}

// itermae! Requires arguments to be specified outside the fuction, so that's
// done in the object itermae_arguments
process itermae {
    publishDir 'tmp'
    cpus 16
    memory '16 GB'
    label 'itermae'
    input:
        tuple val(read), val(id), path(input_fastqz), val(arguments)
    output:
        tuple val(read), val(id), path("${id}_${read}_pass.sam")
    shell: 
        '''
        zcat !{input_fastqz} \
            | parallel --pipe -L 10000 \
                'itermae !{arguments} -v ' \
                > !{id}_!{read}_pass.sam \
                2> err
        '''
}

// split each tag
process split_out_codes { 
    publishDir 'tmp'
    cpus 1
    memory '08 GB'
    label 'munge'
    input:
        tuple val(read), val(id), path(sam)
    output:
        tuple val(read), val(id), path("*.codes"), path("*.ids")
    shell:
        '''
        cat !{sam} | cut -f1,10,12 \
            | mawk -F'\t' '{print($1"\t"$3) > $3".ids"; print($2) > $3".codes"}'
        '''
}

// starcode - splitting files by the first SAM tag (expect XI:0 etc)
process starcode { 
    publishDir 'tmp'
    cpus 16
    memory '60 GB'
    label 'starcode'
    input:
        tuple val(which_code), val(which_tool),
            val(starcode_parms), val(read), val(id), path(codes), path(ids)
    output:
        tuple val(id), val(read), val(which_code), path("${read}_${which_code}_starcode.clustered")
    shell:
        '''
        export SORTPARM="--parallel=!{task.cpus} -T ./ -S 30G"
        starcode --threads !{task.cpus} --seq-id -i !{codes} !{starcode_parms} \
            | cut -f1,3 \
            | mawk '{ split($2,a,","); for (i in a){ print a[i]"\t"$1;} }' \
            | sort ${SORTPARM} -k1,1 -n | cut -f2 | paste !{ids} - \
            | sed 's/\t/,/g' \
            > !{read}_!{which_code}_starcode.clustered
        '''
}










/* so bartender again? */
process bartender {
    publishDir 'tmp'
    cpus 16
    memory '60 GB'
    label 'bartender'
    input:
        tuple val(which_code), val(which_tool),
            val(parms), val(read), val(id), path(codes), path(ids)
    output:
        tuple val(id), val(read), val(which_code), 
            path("${id}_${read}_${which_code}_for_bartender"), 
            path("${id}_${read}_${which_code}_barcode.csv"), 
            path("${id}_${read}_${which_code}_cluster.csv")
    shell:
        '''
        paste -d, !{codes} <( sed 's/\t/_/' !{ids}) \
            > !{id}_!{read}_!{which_code}_for_bartender
        bartender_single_com -t !{task.cpus} !{parms} \
            -f !{id}_!{read}_!{which_code}_for_bartender \
            -o !{id}_!{read}_!{which_code} 
        '''
}

process unfold_bartender {
    cpus 16
    memory '20 GB'
    label 'munge'
    publishDir 'tmp'
    input:
        tuple val(id), val(read), val(which_code), 
            path(input), path(barcodes), path(clusters)
    output:
        tuple val(id), val(read), val(which_code),
            path("${read}_${which_code}_bartender.clustered")
    shell:
        '''
        export SORTPARM="--parallel=!{task.cpus} -T ./ -S 20G"
        join -t, -1 3 -2 1 \
            <( tail -n+2 !{barcodes} | sort ${SORTPARM} -t, -k3,3 ) \
            <( tail -n+2 !{clusters} | sort ${SORTPARM} -t, -k1,1 ) \
            | cut -d, -f2,4 \
            | sort ${SORTPARM} -t, -k1,1 \
            | join -t, -j 1 - <( sort ${SORTPARM} -t, -k1,1 !{input} ) \
            | mawk -F, '{print $3","$2}' \
            | sed 's/_/,/' \
            > !{read}_!{which_code}_bartender.clustered
        '''
}




process remerge_codes { 
    publishDir 'tmp'
    cpus 1
    memory '16 GB'
    label 'munge'
    input:
        tuple val(id), val(read), val(which_code), path("*")
    output:
        tuple val(id), path("${id}_clustered.table")
    shell:
        '''
        export SORTPARM="--parallel=!{task.cpus} -T./ -S16G"
        function joinliston1 {
            while (( $# )); do
                if [[ ! -f tmp.table ]]; then
                    cut -f1,3 -d, $1 | sort ${SORTPARM} -t, -k1,1 > tmp.table
                    shift
                else
                    join -j1 -t, tmp.table \
                        <(cut -f1,3 -d, $1 | sort ${SORTPARM} -t, -k1,1) \
                        > tmp.table2
                    mv -f tmp.table2 tmp.table
                    shift
                fi
            done
        }
        export -f joinliston1

        ls *clustered | xargs -I'{}' bash -c 'joinliston1 {}'
        mv tmp.table !{id}_clustered.table
        '''
}

process tabulate_barcode_combos { 
    publishDir 'output'
    cpus 1
    memory '16 GB'
    label 'munge'
    input:
        tuple val(id), path(table)
    output:
        tuple val(id), path("${id}_tabulated.table")
    shell:
        '''
        export SORTPARM="--parallel=!{task.cpus} -T ./ -S 16G"
        cut -d',' -f2- !{table} \
            | sort ${SORTPARM} | uniq -c \
            > !{id}_tabulated.table
        '''
}




